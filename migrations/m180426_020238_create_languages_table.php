<?php

use yii\db\Migration;

/**
 * Handles the creation of table `languages`.
 */
class m180426_020238_create_languages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('languages', [
                'id'   => $this->primaryKey(),
                'name' => $this->string(255)]
        );

        $this->batchInsert('languages', ['name'], [
            ['name' => 'Русский'],
            ['name' => 'English']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('languages');
    }
}
