<?php

use yii\db\Migration;

class m180426_022450_create_indexes_fks_posts extends Migration
{
    public function up()
    {

        $this->createIndex(
            'idx-post-language_id',
            'posts',
            'language_id'
        );

        $this->createIndex(
            'idx-post-author_id',
            'posts',
            'author_id'
        );

        $this->addForeignKey(
            'fk-post-language_id',
            'posts',
            'language_id',
            'languages',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-post-author_id',
            'posts',
            'author_id',
            'authors',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-post-author_id', 'posts');
        $this->dropForeignKey('fk-post-language_id', 'posts');
        $this->dropIndex('idx-post-author_id', 'posts');
        $this->dropIndex('idx-post-language_id', 'posts');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
