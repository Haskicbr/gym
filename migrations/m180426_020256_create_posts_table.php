<?php

use yii\db\Migration;

/**
 * Handles the creation of table `posts`.
 */
class m180426_020256_create_posts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('posts', [
            'id'          => $this->primaryKey(),
            'language_id' => $this->integer(11),
            'author_id'   => $this->integer(11),
            'likes_count' => $this->integer(11),
            'title'       => $this->string(255),
            'text'        => $this->text(),
        ]);


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('posts');
    }
}
