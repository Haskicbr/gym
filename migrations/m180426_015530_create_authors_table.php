<?php

use yii\db\Migration;

/**
 * Handles the creation of table `authors`.
 */
class m180426_015530_create_authors_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('authors', [
                'id'   => $this->primaryKey(),
                'name' => $this->string(255)]
        );

        $this->batchInsert('authors', ['name'], [
            ['name' => "CrazyNews"],
            ["name" => "Чук и Гек"],
            ["name" => "CatFuns"],
            ["name" => "CarDriver"],
            ["name" => "BestPics"],
            ["name" => "ЗОЖ"],
            ["name" => "Вася Пупкин"],
            ["name" => "Готовим со вкусом"],
            ["name" => "Шахтёрская Правда"],
            ["name" => "FunScience"],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('authors');
    }
}
