<?php

namespace app\commands;

use yii\console\Controller;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper as AH;

class GeneratorController extends Controller
{
    const MAX_WRITE_POSTS = 10000;
    const GENERATE_TEXT_TYPE = 'text';
    const GENERATE_TITLE_TYPE = 'title';

    const TITLE_MAX_WORDS = 6;
    const TITLE_MIN_WORDS = 4;

    const TEXT_MIN_WORDS = 5;
    const TEXT_MAX_WORDS = 8;

    const TEXT_MIN_SENTENCE = 3;
    const TEXT_MAX_SENTENCE = 4;

    const TITLE_MIN_SENTENCE = 1;
    const TITLE_MAX_SENTENCE = 1;


    public $message;
    public $posts_counter = 0;
    public $likes_count = 0;

    public $write_data = [

        'ru' => [
            'text'  => [
                "один", "еще", "бы", "такой", "только", "себя",
                "свое", "какой", "когда", "уже", "для", "вот", "кто",
                "да", "говорить", "год", "знать", "мой", "до", "или",
                "если", "время", "рука", "нет", "самый", "ни", "стать",
                "большой", "даже", "другой", "наш", "свой", "ну", "под",
                "где", "дело", "есть", "сам", "раз", "чтобы", "два", "там",
                "чем", "глаз", "жизнь", "первый", "день", "тута", "во", "ничто",
                "потом", "очень", "со", "хотеть", "ли", "при", "голова", "надо",
                "без", "видеть", "идти", "теперь", "тоже", "стоять", "друг",
                "дом", "сейчас", "можно", "после", "слово", "здесь", "думать",
                "место", "спросить", "через", "лицо", "что", "тогда", "ведь",
                "хороший", "каждый", "новый", "жить", "должный", "смотреть",
                "почему", "потому", "сторона", "просто", "нога", "сидеть",
                "понять", "иметь", "конечный", "делать", "вдруг", "над",
                "взять", "никто", "сделать"
            ],
            'title' => ["жесть", "удивительно", "снова", "совсем", "шок", "случай", "сразу", "событие", "начало", "вирус"]
        ],

        'en' => [
            'text'  => [
                "one", "yet", "would", "such", "only", "yourself", "his", "what", "when",
                "already", "for", "behold", "Who", "yes", "speak", "year", "know", "my",
                "before", "or", "if", "time", "arm", "no", "most", "nor", "become", "big",
                "even", "other", "our", "his", "well", "under", "where", "a business", "there is",
                "himself", "time", "that", "two", "there", "than", "eye", "a life", "first", "day",
                "mulberry", "in", "nothing", "later", "highly", "with", "to want", "whether", "at",
                "head", "need", "without", "see", "go", "now", "also", "stand", "friend", "house",
                "now", "can", "after", "word", "here", "think", "a place", "ask", "across", "face",
                "what", "then", "after all", "good", "each", "new", "live", "due", "look", "why",
                "because", "side", "just", "leg", "sit", "understand", "have", "finite", "do",
                "all of a sudden", "above", "to take", "no one", "make"
            ],
            'title' => ["currency", "amazing", "again", "absolutely", "shocking", "case", "immediately", "event", "beginning", "virus"]
        ]
    ];


    /**
     * @return int
     */
    public function getLikesCount()
    {

        for ($post_count = 1000; $post_count <= 10000; $post_count += 1000) {
            if ($this->posts_counter <= $post_count) {

                echo "rand($post_count - 1000, $post_count)";

                return rand($post_count - 1000, $post_count);
            }
        }

        return 0;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getLanguages()
    {

        try {
            return Yii::$app->db->createCommand("SELECT * FROM languages")->queryAll();
        } catch (Exception $e) {
            throw new Exception('ощибка получения языков');
        }
    }

    public function getAuthors()
    {

        try {
            return Yii::$app->db->createCommand("SELECT * FROM authors")->queryAll();
        } catch (Exception $e) {
            throw new Exception('ощибка получения авторов');
        }
    }


    /**
     * @param $max_words
     * @param $max_sentence
     * @param string $type
     * @param string $lang
     * @return string
     */
    public function generateText($min_words, $max_words, $min_sentence, $max_sentence, $type = self::GENERATE_TEXT_TYPE, $lang = 'en')
    {
        $count_words = count($this->write_data[$lang][$type]);

        $text = [];

        for ($sentence_counter = 0; $sentence_counter < rand($min_sentence, $max_sentence); $sentence_counter++) {

            $sentence = [];

            for ($counter = 0; $counter < rand($min_words, $max_words); $counter++) {
                $words_key = rand(0, $count_words - 1);
                $sentence[] = $this->write_data[$lang][$type][$words_key];
            }

            $sentence = implode(" ", $sentence);
            $sentence = preg_replace('/^\[A-zА-я]{0,1}/', strtoupper(substr($sentence, 0, 1)), $sentence);
            $text[] = $sentence .= ($type === self::GENERATE_TEXT_TYPE) ? '.' : '';
        }

        return implode(' ', $text);
    }


    /**
     * @throws Exception
     */
    public function getLanguagesIds()
    {
        $languages = $this->getLanguages();

        foreach ($languages as $language) {

            if ($language["name"] === 'Русский') {
                $ru_id = $language["id"];
            } else if ($language["name"] === 'English') {
                $en_id = $language["id"];
            }
        }


        if (!isset($ru_id) || !isset($en_id)) {
            throw new Exception('ощибка получения id языков');
        }


        return ['ru' => $ru_id, 'en' => $en_id];

    }

    public function actionIndex()
    {
        echo 'generate posts commands';
    }


    /**
     * @throws Exception
     */
    public function actionCreatePosts()
    {
        $authors = $this->getAuthors();
        $lang = ['ru', 'en'];


        for ($count_write = 0; $count_write <= 10000; $count_write++) {
            $cur_lang_code = $lang[rand(0, 1)];
            $cur_author_id = $authors[rand(0, count($authors) - 1)]['id'];

            $lang_ids = $this->getLanguagesIds();
            $lang_id = $lang_ids[$cur_lang_code];

            $text = $this->generateText(
                self::TEXT_MIN_WORDS,
                self::TEXT_MAX_WORDS,
                self::TEXT_MIN_SENTENCE,
                self::TEXT_MAX_SENTENCE,
                self::GENERATE_TEXT_TYPE,
                $cur_lang_code
            );

            $title = $this->generateText(
                self::TITLE_MIN_WORDS,
                self::TITLE_MAX_WORDS,
                1,
                1,
                self::GENERATE_TITLE_TYPE,
                $cur_lang_code
            );


            $insert_data = [
                'title'       => $title,
                'text'        => $text,
                'author_id'   => $cur_author_id,
                'language_id' => $lang_id,
                'likes_count' => $this->getLikesCount()
            ];
            Yii::$app->db->createCommand()
                ->insert('posts', $insert_data)
                ->execute();

            $this->posts_counter++;

            echo "insert \n\r " . print_r($insert_data, true);
        }
    }
}